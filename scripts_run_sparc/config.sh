#!/bin/bash

MODE=""

# usage message
usage ()
{
  echo "select the mode for run SpaRC" 
  echo "Example: $0 --mode cluster"
  exit 0
}

while [ $# -gt 0 ]; do
    case "$1" in
    --mode)
      shift
      MODE=$1
      ;;
    -*)
      echo "Unknown options: $1"
      usage
      ;;
    *)
      break;
      ;;
    esac
    shift
done

##spark env configure:
if [ "$MODE" = single ];then
	echo "loading the single configure"
	SPARK_SUBMIT=$HOME/software/spark/bin/spark-submit
    MASTER=spark://ThinkPad:7077
    TARGET=$SCRIPTPATH/target/scala-2.11/GlobalCluster-assembly-0.2.jar
    LOGS=$SCRIPTPATH/logs
    DATA=$SCRIPTPATH/data/test
    INPUT=$SCRIPTPATH/data/samples
    INPUT_DATA=$INPUT/sample9.seq,$INPUT/sample13.seq,$INPUT/sample16.seq,$INPUT/sample18.seq,$INPUT/sample20.seq,$INPUT/sample28.seq,$INPUT/sample32.seq,$INPUT/sample34.seq,$INPUT/sample38.seq,$INPUT/sample41.seq
    #INPUT_KEY=data/label100
    
elif [ "$MODE" = cluster ];then
	echo "loading the cluster configure"
    SPARK_SUBMIT=spark-submit
    MASTER=yarn
    TARGET=/home/hadoop/SpaRC-assembly.jar
    INPUT_DATA=data1,data2,data3,data4,data5,data6,data7,data8,data9,data10
    SAVERESULTS=s3://wuda-notebook/results
    #INPUT_KEY=label100
fi

##spark-submit options
deploy_mode=client
driver_memory=4G
driver_cores=1
num_executors=2
executor_memory=10G
executor_cores=3
parallelism=100


##performance parameter:
k=31
m=25
w=21
flag=GLOBAL
min_shared_kmer1=2
min_shared_kmer2=0
max_shared_kmers=20000
min_reads_per_cluster=0
max_degree=100
max_iteration=10
weight=edge
filter=0.2
kmercount=10
JARS=1,2,3,4,5,6,7,8,9,10


