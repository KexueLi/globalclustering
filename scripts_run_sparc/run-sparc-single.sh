
CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --deploy-mode $deploy_mode --driver-memory $driver_memory --driver-cores $driver_cores --num-executors $num_executors --executor-memory $executor_memory --executor-cores $executor_cores --conf spark.default.parallelism=$parallelism --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=false --conf spark.speculation=true --class org.jgi.spark.localcluster.tools
EOF`

SeqAddId=$CMD".SeqAddId  $TARGET -i $INPUT_DATA -o $DATA/seqaddid_output --flag $flag "
$SeqAddId > $LOGS/SeqAddId.log

MinimizerMapReads=$CMD".MinimizerMapReads $TARGET -i $DATA/seqaddid_output --gc_minimizer $DATA/minimizer_global_output --minimizer $DATA/minimizer_output -k $k -m $m -w $w --n_iteration 1 --flag $flag "
$MinimizerMapReads > $LOGS/MinimizerMapRead.log

GraphGen2=$CMD".GraphGen2 $TARGET  -i $DATA/minimizer_output -o $DATA/graphgen2_output  --min_shared_kmers  $min_shared_kmer1  --max_degree $max_degree --n_iteration 1 --wait 1 "
$GraphGen2 > $LOGS/GraphGen2.log

GraphLPA3=$CMD".GraphLPA3 $TARGET -i $DATA/graphgen2_output  -o $DATA/graphlpa3_output --min_shared_kmers  $min_shared_kmer1  --max_shared_kmers $max_shared_kmers --min_reads_per_cluster $min_reads_per_cluster --max_iteration $max_iteration  --weight $weight --wait  1 "
$GraphLPA3 > $LOGS/GraphLPA3.log

GlobalClustering=$CMD".GlobalClustering $TARGET --lpa_input $DATA/graphlpa3_output --mini_input $DATA/minimizer_global_output -o $DATA/globalclustering_output --jars $JARS --kmercount $kmercount  --filter $filter  --n_partition 1 --n_block 1 --wait 1 "
$GlobalClustering > $LOGS/GlobalClustering.log

Global_GraphLPA3=$CMD".GraphLPA3 $TARGET -i $DATA/globalclustering_output  -o $DATA/raphlpa3_global_output --min_shared_kmers $min_shared_kmer2  --max_shared_kmers $max_shared_kmers --min_reads_per_cluster $min_reads_per_cluster --max_iteration $max_iteration --weight $weight --wait 1 "
$Global_GraphLPA3 > $LOGS/Global_GraphLPA3.log

CCAddSeq=$CMD".CCAddSeq $TARGET  --reads $DATA/seqaddid_output -i $DATA/raphlpa3_global_output -o $DATA/ccaddseq_output --local_lpa $DATA/graphlpa3_output --flag $flag --wait 1 "
$CCAddSeq > $LOGS/CCAddSeq.log

#Metric=$CMD".Metric $TARGET -l $DATA/ccaddseq_output -k $INPUT_KEY --flag CAMI -o $DATA/metric_output -n 2 "
#$Metric > $LOGS/Metric.log



