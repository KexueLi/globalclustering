
CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --deploy-mode $deploy_mode --driver-memory $driver_memory --driver-cores $driver_cores --num-executors $num_executors --executor-memory $executor_memory --executor-cores $executor_cores --conf spark.default.parallelism=$parallelism --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=false --conf spark.speculation=true --class org.jgi.spark.localcluster.tools
EOF`

SeqAddId=$CMD".SeqAddId  $TARGET -i $INPUT_DATA -o seqaddid_output --flag $flag "
$SeqAddId > SeqAddId.log

MinimizerMapReads=$CMD".MinimizerMapReads $TARGET -i seqaddid_output --gc_minimizer minimizer_global_output --minimizer minimizer_output -k $k -m $m -w $w --n_iteration 1 --flag $flag "
$MinimizerMapReads > MinimizerMapRead.log

GraphGen2=$CMD".GraphGen2 $TARGET  -i minimizer_output -o graphgen2_output  --min_shared_kmers  $min_shared_kmer1  --max_degree $max_degree --n_iteration 1 --wait 1 "
$GraphGen2 > GraphGen2.log

GraphLPA3=$CMD".GraphLPA3 $TARGET -i graphgen2_output  -o graphlpa3_output --min_shared_kmers  $min_shared_kmer1  --max_shared_kmers $max_shared_kmers --min_reads_per_cluster $min_reads_per_cluster --max_iteration $max_iteration  --weight $weight --wait  1 "
$GraphLPA3 > GraphLPA3.log

GlobalClustering=$CMD".GlobalClustering $TARGET --lpa_input graphlpa3_output --mini_input minimizer_global_output -o globalclustering_output --jars $JARS --kmercount $kmercount  --filter $filter  --n_partition 1 --n_block 1 --wait 1 "
$GlobalClustering > GlobalClustering.log

Global_GraphLPA3=$CMD".GraphLPA3 $TARGET -i globalclustering_output  -o graphlpa3_global_output --min_shared_kmers  $min_shared_kmer2  --max_shared_kmers $max_shared_kmers --min_reads_per_cluster $min_reads_per_cluster --max_iteration $max_iteration --weight $weight --wait 1 "
$Global_GraphLPA3 > Global_GraphLPA3.log

CCAddSeq=$CMD".CCAddSeq $TARGET  --reads seqaddid_output -i graphlpa3_global_output -o ccaddseq_output --local_lpa graphlpa3_output --flag $flag --wait 1 "
$CCAddSeq > CCAddSeq.log

#Metric=$CMD".Metric $TARGET -l ccaddseq_output -k $INPUT_KEY --flag CAMI -o metric_output -n 2 "
#$Metric > Metric.log

##save the results
hadoop fs -getmerge ccaddseq_output  ccaddseq
#hadoop fs -getmerge metric_output  metric
aws s3 cp ccaddseq ${SAVERESULTS}/ccaddseq
#aws s3 cp metric ${SAVERESULTS}/metric



