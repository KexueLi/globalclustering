#!/usr/bin/env bash

MODE=""

# usage message
usage ()
{
echo "$0 <select SpaRC options> .seq/.fasta/.fastq...
You can specify any SpaRC parater to tune clustering performance:
  -i --inFile
  -o --outFile
  --min_shared_kmers 
  --max_kmer_count 
  --max_degree 
  ...
Or running time parameter to optimize compute effience:
  --num_executor
  --num_cores
  ...
#TODO: add some description for every parameter
Also, for full SpaRC options: SpaRC -h
"
 exit 0
}

if [ $# -lt 1 ]; then
	usage
fi

# get input parameters
while [ $# -gt 0 ]; do
    case "$1" in
    --mode)
      shift
      MODE=$1
      ;;
    -*)
      echo "Unknown options: $1"
      usage
      ;;
    *)
      break;
      ;;
    esac
    shift
done

##locate the dir
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd )"
PATH=$SCRIPTPATH:$PATH
echo $SCRIPTPATH
##load configure
CONFIG=$SCRIPTPATH/config.sh
. $CONFIG

if [ "$MODE" = single ];then
	echo "run sparc on single mode..."
	. $SCRIPTPATH/run-sparc-single.sh
elif [ "$MODE" = cluster ];then
	echo "run sparc on cluster mode..."
	. $SCRIPTPATH/run-sparc-cluster.sh
fi



