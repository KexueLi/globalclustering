. `dirname $0`/load_config.sh

READS=$OUTPUT_PREFIX/SeqAddId_output
INPUT=$OUTPUT_PREFIX/GraphLPA_global_output
OUTPUT=$OUTPUT_PREFIX/CCAdd_output
WAIT=1

CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --driver-memory $DRIVER --conf spark.default.parallelism=$PL --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.CCAddSeq  $TARGET \
--reads $READS -i $INPUT --local_file tmp/GraphLPA_output -o $OUTPUT  --flag $FLAG
EOF`

echo $CMD
$CMD