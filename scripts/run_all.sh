#!/bin/bash 

BASEDIR=$(dirname "$0")
$BASEDIR/run_seq_add_id.sh 1 && \
$BASEDIR/run_kmr.sh 1  && \
$BASEDIR/run_edges.sh 1 && \
$BASEDIR/run_lpa.sh 1 && \
$BASEDIR/run_global.sh 1 && \
$BASEDIR/run_lpa_global.sh 1 && \
$BASEDIR/run_cc_add_seq.sh  1


