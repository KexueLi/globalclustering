. `dirname $0`/load_config.sh
INPUT=$OUTPUT_PREFIX/Global_output
OUTPUT=$OUTPUT_PREFIX/GraphLPA_global_output
WEIGHT=edge
WAIT=1

CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --driver-memory $DRIVER --conf spark.default.parallelism=$PL --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.GraphLPA3  $TARGET \
--wait $WAIT  -i $INPUT  -o $OUTPUT --min_shared_kmers $min_shared_kmers2  --max_shared_kmers $max_shared_kmers --min_reads_per_cluster $min_reads_per_cluster --max_iteration $max_iteration --weight $WEIGHT
EOF`

echo $CMD
$CMD