. `dirname $0`/load_config.sh
INPUT=$OUTPUT_PREFIX/SeqAddId_output
OUTPUT=$OUTPUT_PREFIX/minimizer
KMER=$OUTPUT_PREFIX/kmer
WAIT=1

CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --driver-memory $DRIVER --conf spark.executor.extraClassPath=$TARGET  --conf spark.driver.maxResultSize=8g --conf spark.network.timeout=360000 --conf spark.speculation=true --conf spark.default.parallelism=$PL --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.executor.userClassPathFirst=true --conf spark.driver.userClassPathFirst=true $TARGET  \
MinimizerMapReads --wait $WAIT -i $INPUT  --format seq --minimizer $OUTPUT  -k $k -m $m --kmer $KMER --min_kmer_count $min_kmer_count --max_kmer_count $max_kmer_count --n_iteration 1 --flag $FLAG
EOF`

echo $CMD
$CMD
