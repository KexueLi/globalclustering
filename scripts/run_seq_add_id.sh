. `dirname $0`/load_config.sh
INPUT=data/samples/sample9.seq,data/samples/sample13.seq,data/samples/sample16.seq,data/samples/sample18.seq,data/samples/sample20.seq,data/samples/sample28.seq,data/samples/sample32.seq,data/samples/sample34.seq,data/samples/sample38.seq,data/samples/sample41.seq

OUTPUT=$OUTPUT_PREFIX/SeqAddId_output
WAIT=1

CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --driver-memory $DRIVER --conf spark.default.parallelism=$PL --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.SeqAddId  $TARGET \
-i $INPUT  -o $OUTPUT --flag $FLAG
EOF`

echo $CMD
$CMD
