. `dirname $0`/load_config.sh
INPUT=$OUTPUT_PREFIX/GraphLPA_output
OUTPUT=$OUTPUT_PREFIX/Global_output
WEIGHT=none
WAIT=1

CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --driver-memory $DRIVER --conf spark.default.parallelism=$PL --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.GlobalClustering  $TARGET \
--mini_file tmp/kmer -i $INPUT  -o $OUTPUT --jars 9,13,16,18,20,28,32,34,38,41 --kmerCount 10 --filter 0.2  --n_partition 1 --n_block 1 --wait 0
EOF`

echo $CMD
$CMD