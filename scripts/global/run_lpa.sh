. `dirname $0`/load_config_gc.sh
INPUT=$GraphGen2_OUTPUT
OUTPUT=$GraphLPA3_OUTPUT

CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --deploy-mode client --driver-memory $DRIVER_MEMORY --driver-cores 1 --executor-memory 1G --executor-cores 1 --conf spark.executor.extraClassPath=GlobalCluster-assembly-0.2.jar --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.default.parallelism=$PL --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.GraphLPA3  $TARGET \
--wait  1  -i $INPUT  -o $OUTPUT --min_shared_kmers  2  --max_shared_kmers $max_shared_kmers --min_reads_per_cluster $min_reads_per_cluster --max_iteration $max_iteration  --weight $weight 
EOF`

echo $CMD
$CMD

    
