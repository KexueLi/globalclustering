. `dirname $0`/load_config_gc.sh
READS=$SeqAddId_OUTPUT
LOCAL_LPA=$GraphLPA3_OUTPUT
INPUT=$GraphLPA3_Global_OUTPUT
OUTPUT=$CCAddSeq_OUTPUT

 
CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --deploy-mode client --driver-memory $DRIVER_MEMORY --driver-cores 1 --executor-memory 1G --executor-cores 1 --conf spark.executor.extraClassPath=GlobalCluster-assembly-0.2.jar --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.default.parallelism=$PL --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.CCAddSeq  $TARGET  \
--wait 1 --flag $flag --reads $READS -i $INPUT -o $OUTPUT --local_lpa $LOCAL_LPA -p *.seq
EOF`

echo $CMD
$CMD
