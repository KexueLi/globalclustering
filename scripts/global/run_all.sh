#!/bin/bash 

BASEDIR=$(dirname "$0")
$BASEDIR/scripts_gc/run_add_seq.sh 2>&1 | tee logs/seqaddid.log && \
$BASEDIR/scripts_gc/run_mini.sh 2>&1 | tee logs/minimizer.log && \
$BASEDIR/scripts_gc/run_edges.sh 2>&1 | tee logs/graphgen2.log && \
$BASEDIR/scripts_gc/run_lpa.sh 2>&1 | tee logs/graphlpa3.log && \
$BASEDIR/scripts_gc/run_gc.sh 2>&1 | tee logs/globalclustering.log && \
$BASEDIR/scripts_gc/run_gc_lpa.sh 2>&1 | tee logs/gc_graphlpa3.log && \
$BASEDIR/scripts_gc/run_add_cc.sh 2>&1 | tee logs/ccaddseq.log


