. `dirname $0`/load_config_gc.sh
LPA_INPUT=$GraphLPA3_OUTPUT
MINI_INPUT=$MinimizerMapReads_Global
OUTPUT=$GlobalClustering_OUTPUT
JAR=$JARS


CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --deploy-mode client --driver-memory $DRIVER_MEMORY --driver-cores 1 --executor-memory 1G --executor-cores 1 --conf spark.executor.extraClassPath=GlobalCluster-assembly-0.2.jar --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.default.parallelism=$PL --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.GlobalClustering  $TARGET \
--wait 1 --lpa_input $LPA_INPUT --mini_input  $MINI_INPUT -o $OUTPUT --jars $JAR --kmercount 10  --filter 0.2  --n_partition 1 --n_block 1
EOF`

echo $CMD
$CMD


