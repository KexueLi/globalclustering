. `dirname $0`/load_config_gc.sh
INPUT_LPA=data/CCAdd_output
INPUT_KEY=data/label100
OUTPUT=tmp/Metric_out


CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --deploy-mode client --driver-memory $DRIVER_MEMORY --driver-cores 1 --executor-memory 1G --executor-cores 1 --conf spark.executor.extraClassPath=GlobalCluster-assembly-0.2.jar --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.default.parallelism=$PL --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.Metric $TARGET \
-l $INPUT_LPA  -k $INPUT_KEY --flag CAMI -o $OUTPUT -n 2
EOF`

echo $CMD
$CMD
