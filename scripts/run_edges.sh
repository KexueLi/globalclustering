. `dirname $0`/load_config.sh
INPUT=${OUTPUT_PREFIX}/minimizer
OUTPUT=$OUTPUT_PREFIX/GraphGen2_output
WAIT=1

CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER  --driver-memory $DRIVER --conf spark.executor.extraClassPath=$TARGET --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.speculation=true  --conf spark.default.parallelism=$PL --conf spark.eventLog.enabled=$ENABLE_LOG  $TARGET \
GraphGen2 --wait $WAIT -i $INPUT -o $OUTPUT  --min_shared_kmers $min_shared_kmers1 --max_degree $max_degree
EOF`

echo $CMD
$CMD