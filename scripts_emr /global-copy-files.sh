#!/bin/bash

set -x -e

while [ $# -gt 0 ]; do
    case "$1" in
    --put-data)
      shift
      PUT_DATA=$1
      ;;
    --key-data)
      shift
      KEY_JARS=$1
      ;;
    -*)
      # do not exit out, just note failure
      error_msg "unrecognized option: $1"
      ;;
    *)
      break;
      ;;
    esac
    shift
done


aws s3 cp $PUT_DATA input_data
aws s3 cp $KEY_DATA key_data

hadoop fs -put input_data
hadoop fs -put key_data







