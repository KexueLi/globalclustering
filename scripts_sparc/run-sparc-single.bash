#!/bin/bash

#translate format
if [ "$FORMAT" = fasta ];
then 
 	echo "do fasta2seq"
	python ${SCRIPTPATH}/fastaToSeq.py -i /home/science/Projects/sparc/data/sample.fasta -o ${SCRIPTPATH}/test.seq -p 1 -s 0
elif [ "$FORMAT" = fastq ];
then
	echo "do fastq2seq"
	python ${SCRIPTPATH}/fastqToSeq.py -i /home/science/Projects/sparc/data/sample.fasta -o ${SCRIPTPATH}/test.seq -p 1 -s 0
elif [ "$FORMAT" = seq ];
then
	echo "already .seq format, do nothing"
fi
CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --driver-memory $DRIVER --conf spark.default.parallelism=$PL --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true --class org.jgi.spark.localcluster.tools
EOF`
SeqAddId=$CMD".SeqAddId  $TARGET -i $INPUT -o $OUTPUT_PREFIX/mockAddID --flag $FLAG  "
$SeqAddId > $LOGS/SeqAddID.log
MinimizerMapReads=$CMD".MinimizerMapReads $TARGET -i $OUTPUT_PREFIX/mockAddID  --gc_minimizer $OUTPUT_PREFIX/kmer --minimizer $OUTPUT_PREFIX/minimizer -k $k -m $m -w 20 --n_iteration 1"
#$MinimizerMapReads > $LOGS/MinimizerMapRead.log
