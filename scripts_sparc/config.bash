#!/bin/bash

##mode
MODE=single
FLAG=Global

##spark env configure:
if [ $MODE=single ];
then
	OUTPUT_PREFIX=$SCRIPTPATH/tmp/
	INPUT=$SCRIPTPATH/data/mock.seq
	TARGET=$SCRIPTPATH/target/scala-2.11/GlobalCluster-assembly-0.2.jar
	SPARK_SUBMIT=$SPARK_HOME/bin/spark-submit
	LOGS=$SCRIPTPATH/logs
	PL=100
	ENABLE_LOG=false
	MASTER=local[4]
	DRIVER=4G
elif [ $MODE=cluster ];
then
      echo  ##.......
fi

##performance parameter:
k=31
m=15
min_kmer_count=2
max_kmer_count=100000
min_shared_kmers1=2
min_shared_kmers2=0
max_shared_kmers=20000
min_reads_per_cluster=2
max_iteration=10
max_degree=50
K=$k
CL=0


