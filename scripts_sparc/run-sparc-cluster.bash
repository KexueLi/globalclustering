#!/bin/bash

##translate format
if [ "$FORMAT" = fasta ];
then 
 	echo "do fasta2seq"
	python ${SCRIPTPATH}/fastaToSeq.py -i /home/science/Projects/sparc/data/sample.fasta -o ${SCRIPTPATH}/test.seq -p 1 -s 0
elif [ "$FORMAT" = fastq ];
then
	echo "do fastq2seq"
	python ${SCRIPTPATH}/fastqToSeq.py -i /home/science/Projects/sparc/data/sample.fasta -o ${SCRIPTPATH}/test.seq -p 1 -s 0
elif [ "$FORMAT" = seq ];
then
	echo "already .seq format, do nothing"
fi
#hadoop fs -put -D  ....
CMD=`cat<<EOF
#.....
#hadoop get merge
