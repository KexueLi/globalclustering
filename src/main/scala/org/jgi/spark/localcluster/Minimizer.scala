package org.jgi.spark.localcluster

import com.typesafe.scalalogging.LazyLogging
import org.jgi.spark.localcluster.tools.MinimizerMapReads.Config

object Minimizer extends LazyLogging {


  def logInfo(str: String) = {
    println(str)
    logger.info(str)
  }

  private def canonical_kmer(seq: String) = {
    val rc = DNASeq.reverse_complement(seq)
    if (rc < seq) rc else seq
  }


  def main(args: Array[String]) ={

    var k=31
    var m=15

    var seq="ATCGGACTTTTACGATACGGAAAGTTCGTTCAGAACCGTTACCAGATTAGGAAACCCATGTACAAAGTCAGTAAGTCAAGTCCAGTCCCAAAAGTCCCAA"

    k=13
    m=7
    seq="ATCGTCGTAGATCGTCTGATAGTGA"
    println("the seq length is "+seq.length)

    val w = 7
    val k1 = 7
    val a=generate_mmers3(seq, w, k1)
    println("the result is "+a.mkString("，"))
    val b=a.map{i=>i.to_base64}
    println("the number of minimizer is "+b.length)

  }


  //2.add the end minimizer
  def generate_mmers3(seq: String, w: Int, k: Int): Array[DNASeq] = {
    seq.split("N").flatMap {
      subSeq => {
        val a=_generate_w_kmer3(subSeq, w, k)
        a
      }
    }.distinct
  }

  //generate w adjacent kmers (sequential window)given the string
  private def _generate_w_kmer3(seq: String, w: Int , k: Int) :IndexedSeq[DNASeq]= {
    val window_len = w + k - 1
    val string_length = seq.length

    if (string_length > window_len) {
      val first_letter = string_length - window_len + 1
      val first_letter2 = string_length - k+1
      val end_minimizer_right = (first_letter until first_letter2).map {
        j =>
          val kmer_sequnce_window = seq.substring(j)
          val minimizer = generate_minimizer3(kmer_sequnce_window, k)
          val a = DNASeq.from_bases(minimizer)
          a
      }


      val end_minimizer_left = (k until window_len ).map {
        j =>
          val kmer_sequnce_window = seq.substring(0, j )
          val minimizer = generate_minimizer3(kmer_sequnce_window, k)
          val a = DNASeq.from_bases(minimizer)
          a
      }

      val w_k_minimizer = (0 until (seq.length - window_len + 1)).map {
        i =>
          val kmer_sequnce_window = seq.substring(i, i + window_len)
          val minimizer = generate_minimizer3(kmer_sequnce_window, k)
          val a = DNASeq.from_bases(minimizer)
          a
      }

      val z = end_minimizer_left.union(w_k_minimizer).union(end_minimizer_right)
      z
    } else {
      logInfo("There is a string with a length less than w+k-1. ")
      val w_k_minimizer = (0 until (string_length - window_len + 1)).map {
        i =>
          val kmer_sequnce_window = seq.substring(i, i + window_len)
          val minimizer = generate_minimizer3(kmer_sequnce_window, k)
          val a = DNASeq.from_bases(minimizer)
          a
      }
      w_k_minimizer
    }
  }

  // generate minimizer from window ,reverse complement
  private def generate_minimizer3(kmer: String, k: Int):String= {
    if(kmer.length == k){
      kmer
    }else{
      val krc = DNASeq.reverse_complement(kmer)
      val minimizers = (
        (0 until (kmer.length - k + 1)).map(i => kmer.substring(i,i+k))
          ++ (0 until (krc.length - k + 1)).map(i => krc.substring(i,i+k))
        ).sorted

      val mn=minimizers(0)
      mn
    }
  }
  
    //copy from the myminimizer
    def generate_minimizer(config: Config,id:(Long,String),kmer: String):String= {
    // generate minimizer from a k-mer
    val m=config.m
    val krc = DNASeq.reverse_complement(kmer)
    val minimizers = (
      (0 until (kmer.length - m + 1)).map(i => kmer.substring(i,i+m))
        ++ (0 until (krc.length - m + 1)).map(i => krc.substring(i,i+m))
      ).sorted
    val mn=minimizers(0)
    //println("the minimizer  "+mn)
    mn
  }

  def generate_kmers(config: Config,id:(Long,String),seq: String, k: Int, m: Int): Array[DNASeq] = {
    seq.split("R|Y|M|K|S|W|H|B|V|D|Y|N").flatMap {
      subSeq => {
        (0 until (subSeq.length - k + 1)).map {
          i =>
            val kmer = subSeq.substring(i, i + k)
            val a = DNASeq.from_bases(kmer)
            a
        }
      }.distinct
    }
  }


}
