/**
  * Created by Lizhen Shi on 6/8/17.
  */
package org.jgi.spark.localcluster.tools

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}
import org.jgi.spark.localcluster.{DNASeq, Utils}
import sext._


object SeqAddId extends App with LazyLogging {

  case class Config(output: String = "", input: Seq[String] = Seq(),flag:String = "",
                    n_partition: Int = -1, shuffle: Boolean = false)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("SeqAddId") {
      head("SeqAddId", Utils.VERSION)

      opt[String]("flag").valueName("<string>").action((x, c) =>
        c.copy(flag = x)).
        validate(x =>
          if (Seq("local","global").contains(x.toLowerCase)) success
          else failure("should be one of <local|global>"))
        .text("clustering schema. <local|global>")

      opt[Seq[String]]('i',"input").valueName("<sample1.seq>,<sample2.seq>...").action( (x,c) =>
        c.copy(input = x) ).text("sample input")

      opt[String]('o', "output").required().valueName("<dir>").action((x, c) =>
        c.copy(output = x)).text("output file")

      opt[Int]('n', "n_partition").action((x, c) =>
        c.copy(n_partition = x))
        .text("paritions of output")

      help("help").text("prints this usage text")

    }
    parser.parse(args, Config())
  }

  def run(config: Config, sc: SparkContext): Unit = {

    val start = System.currentTimeMillis
    logger.info(new java.util.Date(start) + ": Program started ...")

    if(config.flag.toLowerCase=="global") {
      println("Global clustering is running...")
      /*
      val rdd = 0.until(config.input.length).map {
        i =>
          println(config.input(i))
          sc.textFile(config.input(i))
            .map {
              line => line.split("\t|S|R|/|\n")
            }.map { x => (x(1), ("S" + x(1) + "R" + x(2)), x(3)) }
      }.toSeq

      KmerCounting.delete_hdfs_file(config.output)
      sc.union(rdd).zipWithIndex().map(x => x._2 + "\t" + x._1._1 + "\t" + x._1._2 + "\t" + x._1._3)
        .saveAsTextFile(config.output)
      */


      val rdd = 0.until(config.input.length).map {
        i =>
          println(config.input(i))
          sc.textFile(config.input(i))
            .map {
              line => line.split("\t|\n")
            }.map { x => (i+1, x(0), x(1)) }
      }.toSeq
      KmerCounting.delete_hdfs_file(config.output)
      sc.union(rdd).zipWithIndex().map(x => x._2 + "\t" + x._1._1 + "\t" + x._1._2 + "\t" + x._1._3 )
        .saveAsTextFile(config.output)


    }else{
      val rdd = 0.until(config.input.length).map {
        i =>
          println(config.input(i))
          sc.textFile(config.input(i))}.toSeq
      KmerCounting.delete_hdfs_file(config.output)
      sc.union(rdd).zipWithIndex().map(u=>u._2.toInt.toString+"\t"+u._1).saveAsTextFile(config.output)

    }

    logger.info(s"save results to ${config.output}")
    val totalTime1 = System.currentTimeMillis
    logger.info("Processing time: %.2f minutes".format((totalTime1 - start).toFloat / 60000))
  }

  override def main(args: Array[String]) {

    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")
        val conf = new SparkConf().setAppName("Spark SeqAddId")
        conf.registerKryoClasses(Array(classOf[DNASeq]))

        val sc = new SparkContext(conf)
        run(config, sc)

        sc.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }
  } //main
}
