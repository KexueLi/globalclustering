/**
  * Created by Kexue Li on 2018/11/27 0022.
  *
  *Global Clustering
  *
  */
package org.jgi.spark.localcluster.tools

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import org.jgi.spark.localcluster._
import sext._
import org.apache.spark.mllib.linalg._

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import org.apache.spark.sql.SparkSession
import org.apache.spark.mllib.stat.Statistics
import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
import org.apache.spark.mllib.linalg.distributed.{MatrixEntry, RowMatrix}


import scala.util.control.Breaks
import util.control.Breaks._
import breeze.linalg.norm
import breeze.numerics._
import shapeless.ops.tuple.Filter

import scala.math

object GlobalClustering extends App with LazyLogging {
  case class Config(mini_input:String="",lpa_input:String="", jars: Seq[Int] = Seq(),
                    kmercount:Int=30,filter: Double = 0.00005,output:String="",
                    n_partition: Int = 10,n_block:Int=1, sleep: Int = 0,pattern: String = "")

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("GlobalClustering") {
      head("GlobalClustering", Utils.VERSION)

      opt[String]("lpa_input").required().valueName("<dir/file>").action((x, c) =>
        c.copy(lpa_input= x)).text("")

      opt[String]("mini_input").required().valueName("<dir/file>").action((x, c) =>
        c.copy(mini_input = x)).text("")

      opt[String]('o',"output").required().valueName("<dir/file>").action((x, c) =>
        c.copy(output = x)).text("")

      opt[Seq[Int]]("jars").valueName("<jar1>,<jar2>...").action( (x,c) =>
        c.copy(jars = x) ).text("jars to include")

      opt[Int]("kmercount").required().valueName("<dir/file>").action((x, c) =>
        c.copy(kmercount = x)).text("the number of samples for input")

      opt[Double]("filter").action((x, c) =>
        c.copy(filter = x))
        .text("the fraction of top k-mers to keep, others are removed likely due to contamination")

      opt[String]('p', "pattern").valueName("<pattern>").action((x, c) =>
        c.copy(pattern = x)).text("")

      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

      opt[Int]('n', "n_partition").action((x, c) =>
        c.copy(n_partition = x))

        .text("paritions for the input, only applicable to local files")

      opt[Int]("n_block").action((x, c) =>
        c.copy(n_block = x))
        .text("")

      help("help").text("prints this usage text")
    }
    parser.parse(args, Config())
  }

  def logInfo(str: String) = {
    println(str)
    logger.info(str)
  }

  def filterCluster(config: Config,sc: SparkContext):RDD[String]={
    val lpaRDD= sc.textFile(config.lpa_input).map {
      line => line.split(",")
    }.map(x=>x.map(_.toInt)).filter(x=>x.length>=0)
    logInfo(s"There are ${lpaRDD.count()} clusters whose size>100")
    val filter_result = lpaRDD.map(_.mkString(","))
    return filter_result
  }

  def typeKmer( sampleRDD:RDD[(String,String,String)],filter_result:RDD[String],
                config: Config,sc: SparkContext):RDD[String] = {
    //将SpaRC聚类后的clusterID和原始的readseq对应起来--->(clusterID,seq)
    println("TypeKmer is running...")
    val clusterRDD= filter_result
      .map(_.split(",")).map(x => x.map(_.toInt)).
      filter(x=>x.length>=0).
      zipWithIndex().map {
      case (nodes, idx) =>
        nodes.map((_, idx.toInt))
    }.flatMap(x => x).map(x=>(x._1.toString,x._2.toString))//SpaRC聚类后(readsID,clusterID)
    val kmerRDD = sampleRDD.map(x=>(x._3,x._1))//(readsID,kmerID)
    val resultRDD = clusterRDD.join(kmerRDD).map(x=>x._2).groupByKey()
      .persist(StorageLevel.MEMORY_AND_DISK)//(clusterID,Iterator(kmer1,kmer2...))
    val kmerPair=resultRDD.map{
      case(id,kmer)=> {
        val kmerNumMap = scala.collection.mutable.Map[String, Int]()
        for (i <- 0 until kmer.size) {
          var temp = kmerNumMap.getOrElse(kmer.toSeq(i),0)
          kmerNumMap.put(kmer.toSeq(i),temp+1)
        }//map1(kmer1->3,kmer2->5)
        val kmerNumFilterMap=kmerNumMap.filter(x=>x._2>0).toList.sortBy(x=>(x._2,x._1))//从小到大相加，中位偏小
        val sumKmerSize=kmerNumFilterMap.map(_._2).reduce(_+_)
        var sum=0
        var i = 0
        while (i < kmerNumFilterMap.size && sum<=sumKmerSize/2.0){
          sum+=kmerNumFilterMap(i)._2
          i=i+1
        }
        val medianKmerSize =kmerNumFilterMap(i-1)._2 //中位kmer序数
        val typicalKmer=
          if(kmerNumFilterMap.isEmpty) List("null")
          else {
            val kmer=kmerNumFilterMap.filter(x=>x._2==medianKmerSize)
              .map(_._1).sortBy(x=>x).take(config.kmercount)
            kmer
          }
        (typicalKmer,id.toLong)
      }
    }.map{
      case(list,id)=>
        val pair=0.until(list.size).map{ i=>(list(i),id)}
        pair
    }.flatMap(x=>x)
    println("kmerPair: ")
    //kmerPair.take(30).foreach(println(_))
    val type_result = kmerPair.map{
      case (kmer,clusterID)=>
        kmer+" "+ clusterID
    }//.persist(StorageLevel.MEMORY_AND_DISK)
    resultRDD.unpersist()

    return type_result
  }

  def abunMatrix(sampleRDD:RDD[(String,String,String)],type_result:RDD[String],
                 config: Config, sc: SparkContext):RDD[String] = {
    //将"好kmer"和minimizer顺带产生的kmer进行匹配,得到每个sample中"好kmer"数量,进而建立kmer abundance matrix
    println("abunMatrix is running...")
    val clusterRDD=type_result.map{
      lines=>lines.split(" |\n")
    }.map(x=>(x(0),x(1).toLong))//(kmer,cluster)
    val samplerdd = sampleRDD.map(x=>(x._1,x._2))//(kmer,sample)
    //type_result.unpersist()
    //TODO:This is a bug,may be need to know the sample in detail.
    val sampleCount=config.jars.length
    println(s"samplecount: ${sampleCount}")
    val indexArray=new Array[Int](sampleCount)
    for (i<-0 until sampleCount)
      yield indexArray(i)=config.jars(i)
    val result =clusterRDD.filter(x=>x._1!="null")//(kmer,id)
    val nullSize=clusterRDD.count()-result.count()
    logInfo(s"There are ${nullSize} clusters have no typical kmers in total ${result.count()} clusters")
    val clusterJoinSample=result.join(samplerdd).map(x=>((x._1,x._2._1),x._2._2)).groupByKey()
      .map(x => (x._1, x._2.toSeq)).persist(StorageLevel.MEMORY_AND_DISK)
    //(kmer,cluster) + (kmer,sample)=((kmer,cluster),(sample1,sample2....))
    println("SamplejoinKmer: ")
    clusterJoinSample.take(1000000).foreach(println(_))
    val stage1=clusterJoinSample.map{ //9个典型kmer的sample count
      case(kmerCluster,samples)=>
        val sampleNumMap = scala.collection.mutable.Map[Int, Int]()
        val sampleCountArray = new Array[Int](sampleCount)
        samples.foreach {
          x =>
            var temp = sampleNumMap.getOrElse(x.toInt, 0)
            sampleNumMap.put(x.toInt, temp + 1)
        }
        sampleNumMap.foreach(x =>sampleCountArray(indexArray.indexOf(x._1)) = x._2)
        (kmerCluster._2.toInt,sampleCountArray)
    }
    val stage2=stage1.groupByKey().map{
      case(cluster,sampleArray)=>
        val clusterCount=sampleArray.size
        val sample_array = new Array[Int](sampleCount+1)
        for(i<-0 until(sampleCount)){
          val cluster_array = new Array[Int](clusterCount)
          for(j<-0 until(clusterCount)){
            cluster_array(j)=sampleArray.toSeq(j)(i)
          }
          val median_temp=cluster_array.sorted
          sample_array(i)=median_temp(clusterCount/2)
          sample_array(sampleCount)=cluster //最后一位是cluster ID
        }
        sample_array
    }
    val AbunMatrix= stage2.sortBy(x=>x.last)
      .map(x=>x.take(sampleCount).map(_.toDouble))
      .map(Vectors.dense(_))
    AbunMatrix.take(10).foreach(println(_))
    val matrix_result = AbunMatrix.map(_.toArray.map(_.toInt).mkString(","))//.persist(StorageLevel.MEMORY_AND_DISK)//.toString()

    return matrix_result
  }

  def cosine(matrix_result:RDD[String],config: Config, sc: SparkContext)={
    val Abun= matrix_result//读入AbunMatrix,并且缓存
      .map(_.split(","))
      .map{x=>
        val result=for(elem<-x) yield elem.toDouble
        result
      }.zipWithIndex().map(x=>(x._1,x._2.toInt)).repartition(config.n_partition)
    //matrix_result.unpersist()
    val AbunBroad = sc.broadcast(Abun.collect())
    import breeze.linalg._
    import breeze.numerics._
    val cosineSimilarity=Abun.flatMap{
      case(array,id)=>
        val a=DenseVector(array)
        val table=AbunBroad.value
        val buf = new ListBuffer[((Int, Int), Double)]()
        for(i<-0 until table.length){
          if(id<table(i)._2) {
            val b = DenseVector(table(i)._1)
            val similar = (a dot b) / (norm(a) * norm(b))
            if(similar>=config.filter)
            {
              buf += (((id, table(i)._2), similar))
            }
          }
        }
        buf
    }.map(x=>(x._1._1,x._1._2,x._2)).filter(x=>x._1!=x._2)
    cosineSimilarity.take(30).foreach(println(_))
    KmerCounting.delete_hdfs_file(config.output)
    cosineSimilarity.map{
      case (kmer1,kmer2,value)=>
        kmer1.toInt+","+kmer2.toInt+","+value.toInt
    }.saveAsTextFile(config.output)

    //return cosine_result

  }
   def run(config: Config, spark: SparkSession): Unit = {
    val sc = spark.sparkContext
    val start = System.currentTimeMillis
    logInfo(new java.util.Date(start) + ": Program started ...")

  /*   val seqRDD=sc.textFile(config.mini_file).map{
       line => line.split("\t|\n")
     }.map { x => (x(2),x(3)) }
     seqRDD.take(10).foreach(println(_))
     val labelRDD=sc.textFile(config.lpa_file).map{
       line => line.split("\t|\n")
     }.map { x => (x(0),x(1)) }.filter(x=>(x._2=="4306262.0"|x._2=="135956.0"))
     labelRDD.take(10).foreach(println(_))
     KmerCounting.delete_hdfs_file(config.output)
     seqRDD.join(labelRDD).map(x=>x._1+"\t"+x._2._1).saveAsTextFile(config.output)*/

      val sampleRDD = sc.textFile(config.mini_input).map {
        line => line.split("\t| |\n")
      }.map { x => (x(0), x(1), x(2)) }.persist(StorageLevel.MEMORY_AND_DISK)//(kmer,sample,read)
      //sampleRDD.collect.foreach(println(_))
      val filter_result = filterCluster(config,sc)
      val type_result = typeKmer(sampleRDD,filter_result,config,sc)
      val matrix_result = abunMatrix(sampleRDD, type_result, config, sc)
      cosine(matrix_result, config, sc)
      sampleRDD.unpersist()
     //TODO LPA

    val totalTime1 = System.currentTimeMillis
    logInfo("Total process time: %.2f minutes".format((totalTime1 - start).toFloat / 60000))
  }


  override def main(args: Array[String]) {
    val APPNAME = "Spark GlobalClustering"

    val options = parse_command_line(args)
    logger.info(s"called with arguments\n${options.valueTreeString}")

    options match {
      case Some(_) =>
        val config = options.get

        logInfo(s"called with arguments\n${options.valueTreeString}")
        val conf = new SparkConf().setAppName(APPNAME).set("spark.kryoserializer.buffer.max", "512m")
        conf.registerKryoClasses(Array(classOf[DNASeq]))

        val spark = SparkSession
          .builder().config(conf)
          .appName(APPNAME)
          .getOrCreate()

        run(config, spark)
        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
        spark.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }
  } //main
}
