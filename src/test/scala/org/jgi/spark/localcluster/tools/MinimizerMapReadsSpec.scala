package org.jgi.spark.localcluster.tools

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{FlatSpec, Matchers, _}
import sext._

class MinimizerMapReadsSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext  {

  "parse command line" should "be good" in {
    val cfg = MinimizerMapReads.parse_command_line("-i data --gc_minimizer data --minimizer  -k 21 -m 11".split(" ")).get
    cfg.reads_input should be("data")
  }

  "Minimizer" should "work on the global files" in {
    val cfg = MinimizerMapReads.parse_command_line(
      ("--flag GLOBAL -i data/seqaddid_output  --gc_minimizer data/global_minimizer " +
        "--minimizer data/minimizer -k 31 -m 25 -w 20 --n_iteration 1").split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    MinimizerMapReads.run(cfg, sc)
    //Thread.sleep(1000 * 10000)
  }

  "Minimizer" should "work on the local files" in {
    val cfg = MinimizerMapReads.parse_command_line(
      ("--flag LOCAL -i data/local/seqaddid_output  --gc_minimizer  data/local/minimizer " +
        "--minimizer data/local/minimizer -k 31 -m 25 -w 20 --n_iteration 1").split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    MinimizerMapReads.run(cfg, sc)
    //Thread.sleep(1000 * 10000)
  }
}
