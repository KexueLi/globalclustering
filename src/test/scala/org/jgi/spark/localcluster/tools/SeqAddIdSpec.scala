package org.jgi.spark.localcluster.tools

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{FlatSpec, Matchers, _}
import sext._

/**
  * Created by Lizhen Shi on 5/17/17.
  */
class SeqAddIdSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext
{
  "parse command line" should "be good" in {
    val cfg = SeqAddId.parse_command_line("--in data -o data/SeqAddId_output".split(" ")).get
    cfg.input should be("data")
  }

  "SampleAddIdSpec" should "work on the global files" in {
    val cfg = SeqAddId.parse_command_line(
      ("-i data/samples/sample9.seq,data/samples/sample13.seq,data/samples/sample16.seq," +
        "data/samples/sample18.seq,data/samples/sample20.seq,data/samples/sample28.seq," +
        "data/samples/sample32.seq,data/samples/sample34.seq,data/samples/sample38.seq,data/samples/sample41.seq "+
        "-o data/seqaddid_output -n 1 --flag GLOBAL "
        ).split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    SeqAddId.run(cfg, sc)
    //Thread.sleep(1000 * 10000)
  }

  "SampleAddIdSpec" should "work on the local files" in {
    val cfg = SeqAddId.parse_command_line(
      ("-i data/mock.seq "+
        "-o data/local/seqaddid_output -n 1 --flag LOCAL "
        ).split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    SeqAddId.run(cfg, sc)
    //Thread.sleep(1000 * 10000)
  }
}
