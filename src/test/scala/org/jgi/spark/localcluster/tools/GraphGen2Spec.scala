package org.jgi.spark.localcluster.tools

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{FlatSpec, Matchers, _}
import sext._

/**
  * Created by Lizhen Shi on 5/17/17.
  */
class GraphGen2Spec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  "parse command line" should "be good" in {
    val cfg = GraphGen2.parse_command_line("-i data/minimizer -o data/GraphGen2_output".split(" ")).get
    cfg.kmer_reads should be("data")
  }

  "graph gen" should "work on the global files" in {
    val cfg = GraphGen2.parse_command_line(
      "-i data/minimizer -o data/graphgen2_output --n_iteration 1".split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    GraphGen2.run(cfg, sc)

  }

  "graph gen" should "work on the local files" in {
    val cfg = GraphGen2.parse_command_line(
      "-i data/local/minimizer -o data/local/graphgen2_output --n_iteration 1".split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    GraphGen2.run(cfg, sc)

  }
}
