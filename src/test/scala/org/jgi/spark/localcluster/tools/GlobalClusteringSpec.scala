package org.jgi.spark.localcluster.tools

import com.holdenkarau.spark.testing.{DataFrameSuiteBase, SharedSparkContext}
import org.scalatest.{FlatSpec, Matchers, _}
import sext._

/**
  * Created by Lizhen Shi on 5/17/17.
  */

class GlobalClusteringSpec extends FlatSpec with Matchers with BeforeAndAfter with DataFrameSuiteBase {

  "parse command line" should "be good" in {
    val cfg = GlobalClustering.parse_command_line("--mini_file data/Global_minimizer --lpa_input data/GraphLPA_output --kmerCount 10 -o tmp/Global_Output ".split(" ")).get
    cfg.mini_input should be("data/Global_minimizer")
    cfg.lpa_input should be("data/Global_lpa")
  }

  "GlobalClustering" should "work on the global files" in {
    val cfg = GlobalClustering.parse_command_line(
      ("--mini_input data/global_minimizer --lpa_input data/graphlpa3_output  -o data/global_output " +
        " --jars 1,2,3,4,5,6,7,8,9,10 --kmercount 10 --filter 0.2  --n_partition 1 --n_block 1 --wait 0 ").split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    GlobalClustering.run(cfg,spark)
    //Thread.sleep(1000 * 10000)
  }

}

/*--jars 9,13,16,18,20,28,32,34,38,41*/