package org.jgi.spark.localcluster.tools

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.scalatest.{FlatSpec, Matchers, _}
import sext._


class GraphLPA3Spec extends FlatSpec with Matchers with BeforeAndAfter with DataFrameSuiteBase {

  "parse command line" should "be good" in {
    val cfg = GraphLPA3.parse_command_line("-i data -o data".split(" ")).get
    cfg.edge_file should be("data")
  }

  "GraphLPA3" should "work on the global files" in {
    val cfg = GraphLPA3.parse_command_line(
      "-i data/graphgen2_output   -o data/graphlpa3_output --max_iteration 10  --min_reads_per_cluster 0".split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    GraphLPA3.run(cfg, spark)
    //Thread.sleep(1000 * 10000)
  }

  "GraphLPA3" should "work on the global files with lpa" in {

    val cfg = GraphLPA3.parse_command_line(
      "-i data/global_output   -o data/graphlpa3_global_output --max_iteration 10  --min_reads_per_cluster 0 --weight edge --min_shared_kmers 0 ".split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    GraphLPA3.run(cfg, spark)
    //Thread.sleep(1000 * 10000)
  }

  "GraphLPA3" should "work on the local files " in {

    val cfg = GraphLPA3.parse_command_line(
      "-i data/local/graphgen2_output  -o data/local/graphlpa3_output --max_iteration 10  --min_reads_per_cluster 0 --weight edge --min_shared_kmers 0 ".split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    GraphLPA3.run(cfg, spark)
    //Thread.sleep(1000 * 10000)
  }
}
